import { AppComponent } from '../../src/Component/App';
import { mount } from 'enzyme';

// -----------------------------------------------------------------------------

it('App to be the right shape', () => {
	const wrap = mount(<AppComponent />);
	expect(wrap).toMatchSnapshot();
});
