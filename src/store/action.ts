import Store from 'store';

// -----------------------------------------------------------------------------

export function upsertResumeData(type: string, id: number, value: string[] | GenericObject) {
	Store.dispatch('resume:data:upsert', { type, id, value });
}
