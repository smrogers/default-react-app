import { createStoreon, StoreonStore } from 'storeon';

import wireUpEvents from './event';

// -----------------------------------------------------------------------------

let storeonDevtools = undefined;
if (ENVIRONMENT === 'development') {
	storeonDevtools = require('storeon/devtools').storeonDevtools;
}

// -----------------------------------------------------------------------------

export default createStoreon([initialStore, wireUpEvents, storeonDevtools]);

// -----------------------------------------------------------------------------

function initialStore(store: StoreonStore): void {
	store.on('@init', initialStoreState);
}

// -----------------------------------------------------------------------------

function initialStoreState() {
	return {};
}
