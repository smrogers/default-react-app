// graphql is cool, but serverless doesn't think so sometimes

import { StoreonStore } from 'storeon';

// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------

export default function wireUpEvents(store: StoreonStore): void {
	store.on('some:event', someEvent);
}

// -----------------------------------------------------------------------------

function someEvent() {}
