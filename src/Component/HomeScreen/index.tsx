export default function HomeScreenComponent() {
	return (
		<div className="page">
			<h1>Home</h1>
			<div>The day begins anew.</div>
		</div>
	);
}
