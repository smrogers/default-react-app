import { render } from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import HomeScreen from '../HomeScreen';

import c from './style.scss';

// -----------------------------------------------------------------------------

export default function renderApp(): void {
	const container = document.createElement('div');
	container.id = 'container';
	document.body.appendChild(container);

	//
	render(<AppComponent />, container);

	// remove loader components
	const loader = document.getElementById('loader');
	if (loader != null) {
		loader.parentNode.removeChild(loader);
	}

	const loaderStyle = document.getElementById('loaderStyle');
	if (loaderStyle != null) {
		loaderStyle.parentNode.removeChild(loaderStyle);
	}
}

// -----------------------------------------------------------------------------

export function AppComponent() {
	return (
		<Router>
			<div className={c.container}>
				<Switch>
					<Route path="/">
						<HomeScreen />
					</Route>
				</Switch>
			</div>
		</Router>
	);
}
