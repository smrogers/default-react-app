import { useEffect, useState } from 'react';

import c from './style.scss';

// -----------------------------------------------------------------------------

const ScreenComponentCache = {};

const ScreenComponentLookup = {
	Home: () => import(/* webpackChunkName: "co-ho" */ 'Component/HomeScreen'),
};

// -----------------------------------------------------------------------------

export default function ScreenLoaderComponent({ screenName, screenProps = {} }) {
	const [ScreenComponent, setComponent] = useState();
	useEffect(loadScreenComponent(screenName, ScreenComponent, screenProps, setComponent), [
		screenName,
		ScreenComponent,
	]);

	//
	return ScreenComponent != undefined ? (
		ScreenComponent
	) : (
		<div className={c.loader}>
			<svg preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256">
				<circle cx="128" cy="128" r="122" />
			</svg>
		</div>
	);
}

// -----------------------------------------------------------------------------

function loadScreenComponent(
	screenName: string,
	ScreenComponent: Function,
	screenProps: GenericObject,
	setComponent: Function,
) {
	return () => {
		if (ScreenComponent != undefined) {
			setComponent(<ScreenComponent {...screenProps} />);
			return;
		}

		//
		const loader = ScreenComponentLookup[screenName];
		if (loader == undefined) {
			throw new Error(`No loader for ${screenName}`);
		}

		//
		loader().then((result: GenericObject = {}) => {
			const { default: Component } = result;
			if (Component == undefined) {
				throw new Error(`No component loadable for ${screenName}`);
			}

			//
			ScreenComponentCache[screenName] = Component;
			setComponent(<Component {...screenProps} />);
		});
	};
}
